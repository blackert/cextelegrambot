import logging
import time
from telegram.ext import Updater
from telegram.ext import CommandHandler, MessageHandler
from telegram.ext import Filters
from telegram.error import (TelegramError, Unauthorized, BadRequest, 
    TimedOut, ChatMigrated, NetworkError)

from .conf import TelegramBotConfig

class _TelegramBotBaseClass:
    ADMIN_CHAT = 863269646

    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.updater = Updater(token=TelegramBotConfig.TELEGRAM_TOKEN, use_context=True)
        self.dispatcher = self.updater.dispatcher
        self.bot = self.updater.bot
        self.logger.debug("Acquired updater and dispatcher.")
        self.command_handlers = {
            self._start: 'start'
        }
        self.message_handlers = {
            self._echo: Filters.text,
            # Unknown should be the last.
            self._unknown_command: Filters.command,
            self._unknown_message: Filters.text
        }
        self.error_handlers = {
            self._error_callback: None
        }

    def __approve_user(self, chat_id):
        return chat_id in TelegramBotConfig.WHITELISTED_CHATS
    
    def __send_message(self, bot, chat_id, message):
        if self.__approve_user(chat_id):
            bot.send_message(chat_id=chat_id, text=message)
        else:
            self.logger.warn("Non-whitelisted chat sent a message!")
    
    def send_message(self, chat_id, message):
        self.__send_message(self.bot, chat_id, message)

    def _start(self, update, context):
        self.logger.info("'start' command initiated.")
        bot = context.bot
        chat_id = update.effective_chat.id
        message= "I'm a bot, please talk to me!"
        self.__send_message(bot, chat_id, message)
    
    def _echo(self, update, context):
        self.logger.info("'echo' command initiated.")
        bot = context.bot
        chat_id = update.effective_chat.id
        message= update.message.text
        self.__send_message(bot, chat_id, message)

    def _error_callback(self, update, context):
        self.logger.error("An error was found when processing a message.")
        # TODO: Inform the user of the errors or try to fix them.
        try:
            raise context.error
        except Unauthorized:
            self.logger.error("Bot is not authorized.", exc_info=True)
            # remove update.message.chat_id from conversation list
        except BadRequest:
            self.logger.error("Bot has made a bad request.", exc_info=True)
            # handle malformed requests - read more below!
        except TimedOut:
            self.logger.error("Bot request timed out.", exc_info=True)
            # handle slow connection problems
        except NetworkError:
            self.logger.error("Network error occured.", exc_info=True)
            # handle other connection problems
        except ChatMigrated as e:
            self.logger.error("Chat id of a group has changed. Check {}".format(e), exc_info=True)
            # the chat_id of a group has changed, use e.new_chat_id instead
        except TelegramError:
            self.logger.error("A TelegramError occured.", exc_info=True)
            # handle all other telegram related errors
    
    def _unknown_command(self, update, context):
        self.logger.warn("Unknown command was called.")
        bot = context.bot
        chat_id = update.effective_chat.id
        message= "Sorry, I didn't understand that command."
        self.__send_message(bot, chat_id, message)

    def _unknown_message(self, update, context):
        self.logger.warn("Unknown message was typed.")
        bot = context.bot
        chat_id = update.effective_chat.id
        message= "Sorry, I didn't understand what you meant."
        self.__send_message(bot, chat_id, message)


class TelegramBot(_TelegramBotBaseClass):
    def __init__(self):
        super().__init__()
        self._initialize_handlers()
        self._greet_message()
        self.logger.debug("Dispatcher handlers are initialized.")

        # TODO: Switch to websockets.
        self.logger.info("Starting TelegramBot polling.")
        self.updater.start_polling()
        self.updater.idle()

        #
        # TODO: Create a JobQueue (or do some sort of polling) to publish updates to user
        # https://github.com/python-telegram-bot/python-telegram-bot/wiki/Extensions-%E2%80%93-JobQueue
        # 

    def _greet_message(self):
        message = "Telegram bot is up and running!"
        for chat_id in TelegramBotConfig.ADMIN_CHATS:
            self.bot.send_message(chat_id=chat_id, text=message)

    def _initialize_handlers(self):
        for handler_func, handler_name in self.command_handlers.items():
            handler = CommandHandler(handler_name, handler_func)
            self.dispatcher.add_handler(handler)
        
        for handler_func, handler_filter in self.message_handlers.items():
            handler = MessageHandler(handler_filter, handler_func)
            self.dispatcher.add_handler(handler)
        
        for handler_func, _ in self.error_handlers.items():
            self.dispatcher.add_error_handler(handler_func)

if __name__ == "__main__":
    TelegramBot()
