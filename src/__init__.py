"""
Import API endpoints for easy access.
"""


"""
Telegram bot class used for Telegram integration purposes.
When, TelegramBot class is initialized, it will automatically 
start polling and be ready to be used.

# TelegramBot.send_message(chat_id, message)
"""
from .bot import TelegramBot