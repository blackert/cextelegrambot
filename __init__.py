"""
CEX Telegram Bot package.
This package implements the Binance and Coinbase websockets.
Use only provided API endpoints.
"""

from .src import *


def _logger_init(console=False):
    import logging
    from logging.handlers import RotatingFileHandler
    from pathlib import Path
    import os

    LOGGING_DIR = os.path.join(Path(__file__).parent.absolute(), "logs" + os.sep)
    LOGGING_FILE_NAME = "logs.log"
    file_handler = RotatingFileHandler(LOGGING_DIR + LOGGING_FILE_NAME, maxBytes=50000000, backupCount=5)
    file_handler.setLevel(logging.DEBUG)
    handlers = [file_handler]
    if console:
        console_handler = logging.StreamHandler()
        console_handler.setLevel(logging.WARNING)
        handlers.append(console_handler)
    logging.basicConfig(
        handlers=handlers,
        level=logging.DEBUG,
        format="[%(asctime)s.%(msecs)03d] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s",
        datefmt='%Y-%m-%dT%H:%M:%S'
    )


_logger_init(console=True)
